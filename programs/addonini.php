; <?php/*
[general]
name                        ="LibImap"
version                     ="0.3"
addon_type                  ="LIBRARY"
mysql_character_set_database="latin1,utf8"
charset                     ="UTF-8"
description                 ="IMAP library"
description.fr              ="Librairie IMAP"
long_description.fr         ="README.md"
delete                      =1
longdesc                    =""
ov_version                  ="7.3.0"
php_version                 ="5.1.0"
addon_access_control        =0
author                      ="Cantico ( support@cantico.fr )"
icon                        ="icon.png"
mod_imap                    ="Available"
tags                        ="library,mail"
;*/ ?>