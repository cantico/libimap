<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';

/**
 * @see https://github.com/barbushin/php-imap
 * @author Barbushin Sergey http://linkedin.com/in/barbushin
 *
 */
class Func_Imap extends bab_functionality
{
	protected $imapPath;
	protected $login;
	protected $password;
	protected $serverEncoding;

	protected $mbox;


	/**
	 * Set mailbox infos
	 *
	 * @param	string	$imapPath			Serveur name
	 * @param	string	$login
	 * @param	string	$password
	 * @param	string	$serverEncoding
	 */
	public function setMailBox($imapPath, $login, $password, $serverEncoding = 'utf-8')
	{
			$this->imapPath = $imapPath;
			$this->login = $login;
			$this->password = $password;
			$this->serverEncoding = $serverEncoding;
	}

	/**
	 * Connect to mailbox
	 *
	 * @throws LibImap_Exception
	 */
	public function connect()
	{
		$this->mbox = @$this->imap_open($this->imapPath, $this->login, $this->password);
		if(!$this->mbox) {
			throw new LibImap_Exception('Connection error: ' . imap_last_error());
		}
	}

	/**
	 *
	 */
	public function getMBox()
	{
		return $this->mbox;
	}


	protected function checkConnection() {
		if(!$this->imap_ping($this->mbox)) {
			$this->reconnect();
		}
	}

	protected function reconnect() {
		$this->closeConnection();
		$this->connect();
	}

	/**
	 *  Retourne les informations dans un objet contenant les proprietes suivantes :
	 *
     *	Date - Date de derniere modification du contenu de la boite aux lettres en accord avec la RFC2822
     *	Driver - protocole utilise pour acceder a la boite aux lettres: POP3, IMAP, NNTP.
     *	Mailbox - nom de la boite aux lettres
     *	Nmsgs - nombre de messages de la boite aux lettres
     *	Recent - nombre de messages recents de la boite aux lettres
     *
	 *  Retourne FALSE si une erreur survient.
	 *
	 *
	 * @return stdClass
	 */
	public function getCheck() {
		$this->checkConnection();
		$result = $this->imap_check($this->mbox);

		return $result;
	}


    /**
     * @return object witrh the following properties:
     *     - Date 	date of last change (current datetime)
     *     - Driver 	driver
     *     - Mailbox 	name of the mailbox
     *     - Nmsgs 	number of messages
     *     - Recent 	number of recent messages
     *     - Unread 	number of unread messages
     *     - Deleted 	number of deleted messages
     *     - Size 	mailbox size
     */
    public function getInfo()
    {
        $this->checkConnection();
        $result = $this->imap_mailboxmsginfo($this->mbox);

        return $result;
    }


    /**
     * @see imap_status
     * @return object with properties: messages, recent, unseen, uidnext and uidvalidity.
     */
    public function getStatus()
    {
        $this->checkConnection();
        $result = $this->imap_status($this->mbox, $this->imapPath, SA_ALL);

        return $result;
    }


	/**
	 * get a list of mail ID
	 * @return array
	 */
	public function searchMails($imapCriteria = 'ALL') {
		$this->checkConnection();
		$mailsIds = $this->imap_search($this->mbox, $imapCriteria, SE_UID, $this->serverEncoding);

		return $mailsIds ? $mailsIds : array();
	}


	public function listMailBoxes()
	{
	    $this->checkConnection();
	    list($server) = explode('}', $this->imapPath);
	    $server .= '}';
	    return $this->imap_list($this->mbox, $server, '*');
	}


	/**
	 * Delete Email
	 * @param	int	$mId
	 * @return bool
	 */
	public function deleteMail($mId) {
		$this->checkConnection();
		$this->imap_delete($this->mbox, $mId, FT_UID | CL_EXPUNGE);
		$this->imap_expunge($this->mbox);
		return true;
	}

	public function setMailAsSeen($mId) {
		$this->checkConnection();
		$this->setMailImapFlag($mId, '\\Seen');
	}

	public function setMailImapFlag($mId, $flag) {
		$this->imap_setflag_full($this->mbox, $mId, $flag, ST_UID);
	}

	/**
	 * Get mail headers
	 *
	 *
	 * @return string
	 */
	public function getMailHeaders($mId) {
		$this->checkConnection();
		$headers = $this->imap_fetchheader($this->mbox, $mId, FT_UID);

		if(!$headers) {
			throw new LibImap_Exception('Message with UID "' . $mId . '" not found');
		}
		return $headers;
	}






	/**
	 * Get Email
	 * @return LibImap_Mail
	 */
	public function getMail($mId) {
		$this->checkConnection();

		$mail = new LibImap_Mail($this, $mId);


		return $mail;
	}





	protected function closeConnection() {
		if($this->mbox) {
			$errors = imap_errors();
			if($errors) {
				foreach($errors as $error) {
					trigger_error($error);
				}
			}
			imap_close($this->mbox);
		}
	}

	public function __call($imapFunction, $args) {
		$result = call_user_func_array($imapFunction, $args);
		$errors = imap_errors();
		if($errors) {
			foreach($errors as $error) {
				trigger_error($error);
			}
		}
		return $result;
	}


	public function __destruct() {
		$this->closeConnection();
	}
}



/**
 *
 *
 *
 */
class LibImap_Mail {

	/**
	 *
	 * @var Func_Imap
	 */
	private $mailbox;

	/**
	 *
	 * @var int
	 */
	public $mId;



	/**
	 * List of recipients
	 * @exemple array( email => name, email => name)
	 * @see LibImap_Mail::getTo()
	 * @var array
	 */
	private $to = null;

	/**
	 * Comma separated list of recipient for the TO field
	 * @see LibImap_Mail::getToString()
	 * @var string
	 */
	private $toString = null;

	/**
	 * List of recipients
	 * @exemple array( email => name, email => name)
	 * @var array
	 */
	private $cc = null;

	/**
	 * @exemple array( email => name, email => name)
	 * @var array
	 */
	private $replyTo = null;


	private $head = null;

	private $fetched = false;

	private $textPlain = '';
	private $textHtml = '';
	private $vcard = '';

	/**
	 * list of attachments in LibImap upload directory
	 * @var array	<LibImap_MailAttachment>
	 */
	private $attachments = array();


	/**
	 *
	 * @param Func_Imap $mailbox
	 * @param int 		$mId		Mail ID
	 */
	public function __construct(Func_Imap $mailbox, $mId)
	{
		$this->mailbox = $mailbox;
		$this->mId = $mId;
	}


	/**
	 * @return object
	 */
	protected function getHead() {

		if (null === $this->head)
		{
			$this->head = $this->mailbox->imap_rfc822_parse_headers($this->mailbox->getMailHeaders($this->mId));
		}

		return $this->head;
	}

	/**
	 * Message unique ID from the Message-ID header
	 * @return string
	 */
	public function getMessageID()
	{
		$head = $this->getHead();
		return isset($head->message_id) ? $head->message_id : null;
	}

	/**
	 * Message unique ID from the In-Reply-To header
	 * @return string
	 */
	public function getInReplyTo()
	{
		$head = $this->getHead();
		return isset($head->in_reply_to) ? $head->in_reply_to : null;
	}

	/**
	 * Get ISO DateTime of Email
	 * @return string
	 */
	public function getDate()
	{
		$head = $this->getHead();

		if (!isset($head->date))
		{
			return null;
		}

		return date('Y-m-d H:i:s', strtotime($head->date));
	}

	/**
	 * Get timestamp of Email
	 * @return int
	 */
	public function getTimestamp()
	{
		$head = $this->getHead();

		if (!isset($head->date))
		{
			return null;
		}

		return strtotime($head->date);
	}


	/**
	 * Get email subject
	 * @return string
	 */
	public function getSubject()
	{
		$head = $this->getHead();
		return $this->decodeMimeStr($head->subject);
	}

	/**
	 * Sender name if defined
	 * @return Ambigous <NULL, string>
	 */
	public function getFromName()
	{
		$head = $this->getHead();
		return isset($head->from[0]->personal) ? $this->decodeMimeStr($head->from[0]->personal) : null;

	}

	/**
	 * sender email address
	 * @return string
	 */
	public function getFromAddress()
	{
		$head = $this->getHead();

		if (!isset($head->from[0]->host))
		{
			return strtolower($head->from[0]->mailbox);
		}

		return strtolower($head->from[0]->mailbox . '@' . $head->from[0]->host);
	}



	/**
	 * Get all available informations for sender as a string
	 * @return string
	 */
	public function getFrom()
	{
		$fromname = $this->getFromName();

		if (isset($fromname))
		{
			return $fromname.' <'.$this->getFromAddress().'>';
		}

		return $this->getFromAddress();
	}



	/**
	 * Get recipients for the TO field as an array, ignore undisclosed-recipients
	 * @return array
	 */
	public function getTo()
	{
		if (!isset($this->to))
		{
			$head = $this->getHead();

			$this->to = array();
			$this->toString = '';
			$toStrings = array();

			foreach($head->to as $to) {

				if (!isset($to->host) && 'undisclosed-recipients' === $to->mailbox)
				{
					// undisclosed-recipients : ignore
					continue;
				}

				$toEmail = isset($to->host) ? $to->mailbox . '@' . $to->host : $to->mailbox;
				$toEmail = strtolower($toEmail);
				$toName = isset($to->personal) ? $this->decodeMimeStr($to->personal) : null;
				$toStrings[] = $toName ? "$toName <$toEmail>" : $toEmail;
				$this->to[$toEmail] = $toName;
			}
			$this->toString = implode(', ', $toStrings);

		}

		return $this->to;
	}

	/**
	 * Comma separated list of recipient for the TO field
	 * @return string
	 */
	public function getToString()
	{
		$this->getTo();
		return $this->toString;
	}


	/**
	 * Get recipients for the CC field as an array
	 * @return array
	 */
	public function getCc()
	{
		if (!isset($this->cc))
		{
			$this->cc = array();

			$head = $this->getHead();

			if(isset($head->cc)) {
				foreach($head->cc as $cc) {

					if (!isset($cc->host))
					{
						// undisclosed-recipients or local email : ignore
						continue;
					}

					$address = isset($cc->host) ? $cc->mailbox . '@' . $cc->host : $cc->mailbox;

					$this->cc[strtolower($address)] = isset($cc->personal) ? $this->decodeMimeStr($cc->personal) : null;
				}
			}

		}

		return $this->cc;
	}


	/**
	 * Get recipients for the ReplyTo field as an array
	 * @return array
	 */
	public function getReplyTo()
	{
		if (!isset($this->replyTo))
		{

			$this->replyTo = array();

			if(isset($head->reply_to)) {
				foreach($head->reply_to as $replyTo) {

					$address = isset($replyTo->host) ? $replyTo->mailbox . '@' . $replyTo->host : $replyTo->mailbox;
					$mail->replyTo[strtolower($address)] = isset($replyTo->personal) ? $this->decodeMimeStr($replyTo->personal) : null;
				}
			}

		}

		return $this->replyTo;
	}



	/**
	 * Decode string according to ovidentia database charset
	 * @param string $string
	 * @return string
	 */
	protected function decodeMimeStr($string) {
		$newString = '';
		$elements = $this->mailbox->imap_mime_header_decode($string);
		for($i = 0; $i < count($elements); $i++) {
			if($elements[$i]->charset == 'default') {
				$elements[$i]->charset = 'iso-8859-1';
			}

			if (empty($elements[$i]->charset)) {
				$newString .= $elements[$i]->text;
			} else {
				$newString .= bab_getStringAccordingToDataBase($elements[$i]->text, $elements[$i]->charset);
			}
		}

		return $newString;
	}



	protected function quoteAttachmentFilename($filename) {
		$replace = array('/\s/' => '_', '/[^0-9a-zA-Z_\.]/' => '', '/_+/' => '_', '/(^_)|(_$)/' => '');

		return preg_replace(array_keys($replace), $replace, $filename);
	}

	protected function initMailPart($partStruct, $partNum) {
		$data = $partNum ? $this->mailbox->imap_fetchbody($this->mailbox->getMBox(), $this->mId, $partNum, FT_UID) : $this->mailbox->imap_body($this->mailbox->getMBox(), $this->mId, FT_UID);


		$params = array();
		if(!empty($partStruct->parameters)) {
			foreach($partStruct->parameters as $param) {
				$params[strtolower($param->attribute)] = $param->value;
			}
		}
		if(!empty($partStruct->dparameters)) {
			foreach($partStruct->dparameters as $param) {
				$params[strtolower($param->attribute)] = $param->value;
			}
		}




		if($partStruct->encoding == 1) {

			/**
			 * Replace default imap_utf8 function beacause of the uppercase bug :
			 * @see https://bugs.php.net/bug.php?id=44098
			 */

			//$data = $this->mailbox->imap_utf8($data);

			$data = $this->decodeMimeStr($data);
			$params['charset'] = null;
		}
		elseif($partStruct->encoding == 2) {
			$data = $this->mailbox->imap_binary($data);
		}
		elseif($partStruct->encoding == 3) {
			$data = $this->mailbox->imap_base64($data);
		}
		elseif($partStruct->encoding == 4) {
			$data = $this->mailbox->imap_qprint($data);
		}


		if(!empty($params['charset'])) {
			$data = bab_getStringAccordingToDataBase($data, $params['charset']);
		}

		// attachments

		$filename = false;
		$attachmentId = $partStruct->ifid ? trim($partStruct->id, " <>") : null;
		if(empty($params['filename']) && empty($params['name']) && $attachmentId) {
			$filename = $attachmentId . '.' . strtolower($partStruct->subtype);
		}
		elseif(!empty($params['filename']) || !empty($params['name'])) {
			$filename = !empty($params['filename']) ? $params['filename'] : $params['name'];
			$filename = $this->decodeMimeStr($filename);
			$filename = $this->quoteAttachmentFilename($filename);
		}

		if($filename) {
			$attachment = new LibImap_MailAttachment;

			$attachment->filename = $filename;
			$attachment->data = $data;

			if($attachmentId) {
				$attachment->id = $attachmentId;
			}

			$this->attachments[] = $attachment;
		}


		if($partStruct->type == 0 && $data) {
			if(strtolower($partStruct->subtype) == 'plain') {
				$this->textPlain .= $data;
			}
			else if (strtolower($partStruct->subtype) == 'html') {
				$this->textHtml .= $data;
			}
			else if (strtolower($partStruct->subtype) == 'x-vcard') {
				$this->vcard .= $data;
			}
		}
		elseif($partStruct->type == 2 && $data) {
			$this->textPlain .= trim($data);
		}
		if(!empty($partStruct->parts)) {
			foreach($partStruct->parts as $subpartNum => $subpartStruct) {
				$this->initMailPart($subpartStruct, $partNum . '.' . ($subpartNum + 1));
			}
		}
	}




	/**
	 *
	 */
	private function fetchstructure()
	{
		if (true === $this->fetched)
		{
			return;
		}

		$struct = $this->mailbox->imap_fetchstructure($this->mailbox->getMBox(), $this->mId, FT_UID);

		if(empty($struct->parts)) {
			$this->initMailPart($struct, 0);
		}
		else {
			foreach($struct->parts as $partNum => $partStruct) {
				$this->initMailPart($partStruct, $partNum + 1);
			}
		}

		$this->fetched = true;
	}


	/**
	 *
	 * @return string
	 */
	public function getTextPlain()
	{
		$this->fetchstructure();
		return $this->textPlain;
	}

	/**
	 *
	 * @return string
	 */
	public function getTextHtml()
	{
		$this->fetchstructure();
		return $this->textHtml;
	}


	/**
	 *
	 * @return string
	 */
	public function getVCard()
	{
		$this->fetchstructure();
		return $this->vcard;
	}

	/**
	 *
	 * @return multitype:LibImap_MailAttachment
	 */
	public function getAttachments()
	{
		$this->fetchstructure();
		return $this->attachments;
	}


	/**
	 * Get a list of references
	 * References should contain parents message id but can contain only the previous parent
	 *
	 * @see http://cr.yp.to/immhf/thread.html
	 * @see http://www.jwz.org/doc/threading.html
	 *
	 * @return array
	 */
	public function getReferences()
	{
		$head = $this->getHead();
		if (!isset($head->references) && !isset($head->in_reply_to))
		{
			return array();
		}

		if (isset($head->references))
		{
			$references = $head->references;
		}

		if (empty($references))
		{
			if (!isset($head->in_reply_to))
			{
				return array();
			}

			// compatiblity with writers without reference support
			$references = $head->in_reply_to;
		}

		$references = trim($references);

		$return = array();
		$arr = preg_split('/[\s]+/', $references);
		foreach($arr as $ref)
		{
			$ref = trim($ref);

			if (empty($ref))
			{
				continue;
			}

			$return[] = $ref;
		}

		return $return;
	}


	/**
	 * create embeded images into path
	 */
	public function createImages(bab_Path $basePath)
	{
		foreach($this->attachments as $attachment) {

			/* @var $attachment LibImap_MailAttachment */

			$filename = basename($filepath);
			if(isset($attachment->id)) {
				if (preg_match('/(<img[^>]*?)src=["\']?ci?d:' . preg_quote(bab_toHtml($attachment->id)) . '["\']?/is', $html))
				{
					// l'attachement existe dans la source du message

					if (!$basePath->createDir())
					{
						break;
					}

					$file = clone $basePath;
					$file->push($attachment->filename);

					if (!file_put_contents($file->tostring(), $attachment->data))
					{
						break;
					}
				}
			}
		}
	}



	/**
	 * Get html content without the html tags and replace images attachement URL
	 * @param	string	$baseUrl		if null, embeded images will be stripped from content
	 * @param	array	$stripTags
	 * @return string
	 */
	public function getHtmlContent($baseUrl = null, $stripTags = array('html', 'body', 'head', 'meta', 'title')) {

		$html = $this->getTextHtml();

		if(!$html) {
			return bab_toHtml($this->getTextPlain(), BAB_HTML_ALL);
		}



		foreach($stripTags as $tag) {
			$html = preg_replace('/<\/?' . $tag . '.*?>/is', '', $html);
		}
		$html = trim($html, " \r\n");

		foreach($this->getAttachments() as $attachment) {

			/* @var $attachment LibImap_MailAttachment */

			if(isset($attachment->id)) {

				if (null === $baseUrl)
				{
					$html = preg_replace('/(<img[^>]*?)src=["\']?ci?d:' . preg_quote(bab_toHtml($attachment->id)) . '["\']?([^>]*>)/is', '', $html);
				} else {
					$html = preg_replace('/(<img[^>]*?)src=["\']?ci?d:' . preg_quote(bab_toHtml($attachment->id)) . '["\']?/is', '\\1 src="' . bab_toHtml($baseUrl . $attachment->filename) . '"', $html);
				}
			}
		}

		return $html;
	}




	/**
	 *
	 * @return string
	 */
	public function getReplyToString()
	{
		$replyTo = $this->getReplyTo();

		$arr = array();
		foreach($replyTo as $email => $name)
		{
			$arr[] = $name ? $name.' <'.$email.'>' : $email;
		}

		return implode(', ', $arr);
	}


	/**
	 * @return Widget_Displayable_Interface
	 */
	public function getMetadata()
	{
		$W = bab_Widgets();
		$table = $W->TableView();


		$table->addItem($W->Label(LibImap_translate('Subject')), 0, 0);
		$table->addItem($W->Label($this->getSubject()), 0, 1);

		$table->addItem($W->Label(LibImap_translate('Date')), 1, 0);
		$table->addItem($W->Label(bab_longDate($this->getTimestamp())), 1, 1);

		$table->addItem($W->Label(LibImap_translate('From')), 2, 0);
		$table->addItem($W->Label($this->getFrom()), 2, 1);

		$table->addItem($W->Label(LibImap_translate('To')), 3, 0);
		$table->addItem($W->Label($this->getToString()), 3, 1);

		$table->addItem($W->Label(LibImap_translate('Message-ID')), 4, 0);
		$table->addItem($W->Label($this->getMessageID()), 4, 1);

		if ($replyto = $this->getReplyToString())
		{
			$table->addItem($W->Label(LibImap_translate('Reply to')), 5, 0);
			$table->addItem($W->Label($replyto), 5, 1);
		}


		return $table;
	}
}




class LibImap_MailAttachment
{
	public $filename;
	public $data;
	public $id;
}


class LibImap_Exception extends Exception {
}